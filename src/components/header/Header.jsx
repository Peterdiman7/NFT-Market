import styles from "../header/Header.module.scss";
import { Container, Grid, InputBase } from "@mui/material";

import Logo from "../logo/Logo";
import SearchIcon from '@mui/icons-material/Search';
import Button from '@mui/material/Button';

const Header = () => {
    return(
        <Grid container className={styles.container} >
            <Grid className={styles.logo} item>
                <Logo />
            </Grid>
            <Grid item>
                <InputBase
                startAdornment={<SearchIcon />}
                className={styles.input}
                placeholder="Find items, users and activities" 
                />
            </Grid>
            <Grid item>
                <Container>
                <Button
                    size="small"
                    variant="text"
                    >
                        Home
                    </Button>
                <Button>
                    Activity
                </Button>
                <Button
                variant="contained"
                color="primary"
                >
                    EXPLORE
                </Button>
                </Container>
            </Grid>
        </Grid>
    );
}

export default Header;