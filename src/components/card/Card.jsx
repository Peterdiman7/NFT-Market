import CardC from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardActions from '@mui/material/CardActions';
import FavoriteIcon from '@mui/icons-material/Favorite';
import Chip from '@mui/material/Chip';
import styles from './Card.module.scss';
import Avatar from '../avatar/Avatar';
import millify from 'millify';

export default function Card({name = '', likes = 0, mediaUrl = '/images/avatar.png', 
                              user = {avatarUrl: '/images/avatar.png',verified: false}, 
                              price = '', currency = ''}){
    return(
    <div>
    <CardC>
        <CardHeader 
            avatar={<Avatar url={user.avatarUrl} size={40} verified={user.verified} />} />
        <div>
        <CardMedia 
            className={styles.media}
            component="img"
            image={mediaUrl}
            /> 
        </div>
        <CardActions disableSpacing sx={{ padding: '0.25rem 1rem 1.375rem 1rem' }}>
                <div className={styles.cardAction}>
                    <span className={styles.title}>{name}</span>
                        <span className={styles.price}>{price} {currency}</span> 
                </div>
            <Chip 
                className={styles.likes} 
                icon={<FavoriteIcon className={likes > 0 ? styles.icon : null}/>} 
                label = {millify(likes)} 
                variant="outlined" />
        </CardActions>
    </CardC>
    </div>
    );
}