import Avatar from "../avatar/Avatar";
import styles from "../user/User.module.scss";

export default function User({name = '', info = '', avatar = '', size = 55, verified = false}) {
    return(
        <div className={styles.user}>
            <Avatar />
                <span className={styles.name}>{name}</span>
                <span className={styles.info}>{info}</span>
        </div>
    );
}